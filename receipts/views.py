from django.shortcuts import render, get_object_or_404, redirect
from receipts.forms import AccountForm, ExpenseCategoryForm, ReceiptForm
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def show_receipt(request):
    receipts = Receipt.objects.all()
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)
# above is the main page with "my receipts" with vendor, total, tax, etc. information

# below is a FORM to create a receipt.
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)

# below is to VIEW the category
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "receipts/category_list.html", context)

def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipts/account_list.html", context)

#below is creating a form
@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_category.html", context)
